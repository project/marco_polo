<?php

/**
 * @file
 * Contains marco_polo.module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\NodeInterface;

/**
 * Implements hook_help().
 */
function marco_polo_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the marco_polo module.
    case 'help.page.marco_polo':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Notify a user about other simultaneous users on the current page.') . '</p>';
      $output .= '<h3>' . t('More Information') . '</h3>';
      $output .= '<p>' . t('For more information about this module please visit the <a href="@link">module page</a>.', ['@link' => 'https://www.drupal.org/project/marco_polo']) . '</p>';
      return $output;
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for node entities.
 */
function marco_polo_form_node_form_alter(&$form, FormStateInterface &$form_state, $form_id) {
  // Alter only node edit forms.
  if (strpos($form_id, '_edit_form') === FALSE) {
    return;
  }

  $node = $form_state->getFormObject()->getEntity();
  if ($node instanceof NodeInterface) {
    $config = \Drupal::config('marco_polo.settings');
    $form['#attached']['library'][] = 'marco_polo/core';
    $form['#attached']['drupalSettings']['marco_polo'] = [
      'interval' => $config->get('interval') * 1000,
    ];
  }
}
