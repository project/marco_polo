/**
 * @file
 * JavaScript API for the Marco Polo module.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Registers behaviours related to the ajax request.
   */
  Drupal.behaviors.marcoPolo = {

    interval: null,

    form: 'form.node-form',

    attach: function (context, settings) {
      if (context !== document) {
        return;
      }

      var interval = settings.marco_polo.interval,
        self = this;

      // Prepare div for incoming messages.
      $('<div/>', {id: 'marco-polo-message'}).prependTo(self.form);

      self.marco();

      self.interval = setInterval(function () {
        self.marco('editing')
      }, interval);

      $(self.form).on('submit', function () {
        self.marco('submit');
      });
    },

    marco: function (action = 'editing') {
      var self = this;
      $.ajax({
        url: Drupal.url('marco-polo/marco'),
        type: 'POST',
        dataType: 'json',
        data: {path: drupalSettings.path.currentPath, action: action},
        success: function (response) {
          self.processResponse(response)
        },
      });
    },

    processResponse: function (response) {
      if (response.hasOwnProperty('polo')) {
        $('#marco-polo-message').html(response.polo);
        if (response.submitted) {
          $(this.form + ' input[type=submit]').attr('disabled', 'disabled');
          $(this.form).bind('submit', function (e) {
            e.preventDefault();
          });
          clearInterval(this.interval);
        }
      }
      else {
        $('#marco-polo-message').empty();
      }
    }
  };

})(jQuery, Drupal);
