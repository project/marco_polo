<?php

/**
 * @file
 * Hooks for the marco_polo module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the response message for the AJAX call.
 *
 * @param array $response
 *   Message to alter.
 */
function hook_marco_polo_message_alter(&$response) {
  $response['message'] = format_plural($response['count'],
    'This form is opened by 1 other user.',
    'This form is opened by @count other users.'
  );
}

/**
 * @} End of "addtogroup hooks".
 */
