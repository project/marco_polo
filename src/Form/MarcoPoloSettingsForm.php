<?php

namespace Drupal\marco_polo\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure settings for marco_polo.
 */
class MarcoPoloSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'marco_polo_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['marco_polo.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('marco_polo.settings');

    $form['interval'] = [
      '#type' => 'number',
      '#title' => $this->t('Interval'),
      '#description' => $this->t('How often to perform a check for other users on this page.'),
      '#default_value' => $config->get('interval'),
      '#step' => 1,
      '#field_suffix' => 's',
      '#required' => TRUE,
    ];

    $form['expiry'] = [
      '#type' => 'number',
      '#title' => $this->t('Expiry'),
      '#description' => $this->t('Period of time after which the user is considered to become inactive.'),
      '#default_value' => $config->get('expiry'),
      '#step' => 1,
      '#field_suffix' => 's',
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('marco_polo.settings')
      ->set('interval', $form_state->getValue('interval'))
      ->set('expiry', $form_state->getValue('expiry'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
