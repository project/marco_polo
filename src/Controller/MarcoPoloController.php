<?php

namespace Drupal\marco_polo\Controller;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for Marco Polo module routes.
 */
class MarcoPoloController extends ControllerBase {

  /**
   * The config object for this module.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $config;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The current time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $currentTime;

  /**
   * The request service.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The render object.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * MarcoPoloController constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config object for this module.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The render object.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Component\Datetime\TimeInterface $current_time
   *   The time service.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    AccountInterface $current_user,
    Renderer $renderer,
    ModuleHandlerInterface $module_handler,
    TimeInterface $current_time,
    Request $request,
    CacheBackendInterface $cache_backend,
  ) {
    $this->config = $config_factory->get('marco_polo.settings');
    $this->currentUser = $current_user;
    $this->renderer = $renderer;
    $this->moduleHandler = $module_handler;
    $this->currentTime = $current_time;
    $this->request = $request;
    $this->cacheBackend = $cache_backend;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('renderer'),
      $container->get('module_handler'),
      $container->get('datetime.time'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('cache.default')
    );
  }

  /**
   * Check if someone else is editing a current node.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The node id that needs to be checked.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   *
   * @throws \Exception
   */
  public function marco(Request $request) {
    $current_uid = $this->currentUser->id();
    $current_time = $this->currentTime->getCurrentTime();
    $path = $request->request->get('path');
    $action = $request->request->get('action');

    if (!empty($cache = $this->cacheBackend->get('marco_polo:' . $path))) {
      $active_users = array_filter($cache->data,
        function ($value, $uid) use ($current_uid, $current_time) {
          return $uid != $current_uid && $current_time - $value['last_visited'] <= $this->config->get('expiry');
        }, ARRAY_FILTER_USE_BOTH);
    }
    else {
      $active_users = [];
    }

    $submit_flag = (array_search('submit', array_column($active_users, 'action')) !== FALSE);
    $message = $this->currentUser->hasPermission('view user activity on page') ? $this->getResponseMessage($active_users, $submit_flag) : NULL;
    if (!empty($message)) {
      $response = [
        'polo' => $this->renderer->render($message),
        'submitted' => $submit_flag,
      ];
    }
    else {
      $response = NULL;
    }

    // Record the current user activity for this node.
    $active_users[$current_uid] = [
      'last_visited' => $current_time,
      'action' => $action,
    ];
    $this->cacheBackend->set('marco_polo:' . $path, $active_users, $current_time + $this->config->get('expiry'), ['marco_polo']);

    return new JsonResponse($response);
  }

  /**
   * Prepares a response message based on a number of active users.
   *
   * @param array $active_users
   *   Array of active users.
   * @param string $submit_flag
   *   The submit flag value.
   *
   * @return array|null
   *   Message.
   */
  protected function getResponseMessage($active_users, $submit_flag) {
    if ($submit_flag) {
      return [
        '#markup' => $this->t('Someone else has already submitted this form.'),
        '#prefix' => '<div class="messages messages--warning marco-polo-submit">',
        '#suffix' => '</div>',
      ];
    }
    else {
      $count = count($active_users);
      $response = [
        'count' => $count,
        'message' => $this->formatPlural($count,
          'This page is opened by 1 other user.',
          'This page is opened by @count other users.'
        ),
      ];

      $this->moduleHandler->alter('marco_polo_message', $response);

      return !empty($count) ? [
        '#markup' => $response['message'],
        '#prefix' => '<div class="messages messages--status">',
        '#suffix' => '</div>',
      ] : NULL;
    }
  }

}
